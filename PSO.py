from chords import Component
from constants import *
import constants
from notes import Component2
import midi
import threading

global_fitness_max = INF
global_max_notes = []


def print_logs(it):
        print("iteration №", it)
        print("global_fitness =", global_fitness_max)
        if it % 20 == 0:
            print("global_notes =", global_max_notes)


def pso_for_chords(tonality):
    print("Starting PSO for chords\n\n")
    global global_fitness_max
    global_fitness_max = INF
    global global_max_notes
    constants.W = 1
    a = [Component(notes[tonality], tone[tonality]) for i in range(population)]

    for i in range(population):
        if global_fitness_max > a[i].local_max_fitness:
            global_fitness_max = a[i].local_max_fitness
            global_max_notes = a[i].local_max_notes.copy()

    for iteration in range(iterations):
        for i in range(population):
            a[i].evolution(global_max_notes)
            if global_fitness_max > a[i].local_max_fitness:
                global_fitness_max = a[i].local_max_fitness
                global_max_notes = a[i].local_max_notes.copy()
        constants.W *= 0.99
        print_logs(iteration)


def pso_for_notes(chords, tonality):
    print("Starting PSO for notes\n\n")
    constants.WW = 1
    global global_fitness_max
    global global_max_notes
    global_fitness_max = INF
    b = [Component2(notes2[tonality], chords) for i in range(population2)]

    for i in range(population2):
        if global_fitness_max > b[i].local_max_fitness:
            global_fitness_max = b[i].local_max_fitness
            global_max_notes = b[i].local_max_notes.copy()

    for iteration in range(iterations2):
        for i in range(population2):
            b[i].evolution(global_max_notes)
            if global_fitness_max > b[i].local_max_fitness:
                global_fitness_max = b[i].local_max_fitness
                global_max_notes = b[i].local_max_notes.copy()
        constants.WW *= 0.99
        print_logs(iteration)


def make_music(tonality, improve):
    global global_fitness_max
    global global_max_notes
    while global_fitness_max > 0.35:
        pso_for_chords(tonality)
        if global_fitness_max > 0.35:
            print("\n\n\nBad population, restarting.\n\n\n")

    for i in range(48):
        global_max_notes[i] = round(global_max_notes[i])

    print(global_max_notes)
    chords = []

    for i, j, z, in zip(global_max_notes[0::3], global_max_notes[1::3], global_max_notes[2::3]):
        chords.append([i, j, z])
        print([i, j, z])
    print(chords)
    print('___________')
    global_fitness_max = INF

    while global_fitness_max > 0.35:
        pso_for_notes(chords, tonality)
        if global_fitness_max > 0.35:
            print("\n\n\nBad population, restarting.\n\n\n")

    for i in range(32):
        global_max_notes[i] = round(global_max_notes[i])

    print(global_fitness_max)
    print(global_max_notes)

    result = []
    for i in range(32):
        if i % 2 == 1:
            result.append(chords[i // 2] + [global_max_notes[i]])
        else:
            result.append([global_max_notes[i]])

    midi.get_midi(result, improve, tonality)