from midiutil import MIDIFile


def get_midi(degrees, improve_midi, tonality):
    track = 0
    channel = 0
    time = 0    # In beats
    duration = 1    # In beats
    tempo = 120   # In BPM
    volume = 100  # 0-127, as per the MIDI standard

    my_midi = MIDIFile(1)  # One track, defaults to format 1 (tempo track is created automatically)
    my_midi.addTempo(track, time, tempo)
    improve_midi = int(improve_midi)
    if improve_midi == 1:
        if tonality % 2 == 1:
            for i in range(0, len(degrees)):
                if i % 2 == 1:
                    duration = 4
                else:
                    duration = 1.5
                if i == len(degrees) - 1:
                    duration = 5
                if i % 8 == 0:
                    time += 1
                for j in degrees[i]:
                    my_midi.addNote(track, channel, j, time + i * 2, duration, volume)
        else:
            for i in range(0, len(degrees)):
                for j in degrees[i]:
                    my_midi.addNote(track, channel, j, time + i * 2 / 3, duration, volume)
    else:
        for i in range(0, len(degrees)):
            for j in degrees[i]:
                my_midi.addNote(track, channel, j, time + i, duration, volume)

    with open("new_mid.mid", "wb") as output_file:
        my_midi.writeFile(output_file)
