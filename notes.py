import random
from constants import *


major = [0, 2, 4, 5, 7, 9, 11]
minor = [0, 2, 3, 5, 7, 8, 10]
accords_start = [0, 3, 4, 7, 10]


class Component2:
    chords = None
    notes = None
    velocity = None
    fitness = None
    local_max_fitness = None
    local_max_notes = None
    note_pull = None

    def __init__(self, note_pull, chords):
        self.chords = chords.copy()
        self.note_pull = note_pull
        self.notes = [random.choice(note_pull) for i in range(32)]
        self.local_max_notes = self.notes.copy()
        self.velocity = [0] * 32
        self.fitness = self.fitness_function()
        self.local_max_fitness = self.fitness

    def evolution(self, global_max):
        for i in range(32):
            self.velocity[i] = WW * random.uniform(0, 1) * self.velocity[i] + \
                               CC1 * random.uniform(0, 1) * (self.local_max_notes[i] - self.notes[i]) + \
                               CC2 * random.uniform(0, 1) * (global_max[i] - self.notes[i])
            self.notes[i] = self.notes[i] + self.velocity[i]
        self.fitness = self.fitness_function()
        if self.local_max_fitness > self.fitness:
            self.local_max_fitness = self.fitness
            self.local_max_notes = self.notes

    def in_range(self, x):
        return (x > 72) and (x < 96)

    def closest_note(self, x):
        result = INF
        for note in self.note_pull:
            result = min(result, abs(note - x))
        return result

    def fitness_function(self):
        result = INF
        local_result = 0

        for i in range(32):
            if i % 2 == 1:
                local_result += min(abs(self.chords[i // 2][0] % 12 - self.notes[i] % 12) * 3,
                                    abs(self.chords[i // 2][1] % 12 - self.notes[i] % 12) * 3,
                                    abs(self.chords[i // 2][2] % 12 - self.notes[i] % 12) * 3)
            else:
                local_result += self.closest_note(self.notes[i]) * 3

        """Check difference between adjacent chords"""
        for j in range(0, 32, 2):
            if abs(self.notes[j] - self.notes[j + 1]) > 6:
                local_result += 3 * (abs(self.notes[j] - self.notes[j + 1]) - 6)
        result = min(result, local_result)

        """Check that there are no 3 similar chords that following each other"""
        for j in range(0, 30, 1):
            if (abs(self.notes[j] - self.notes[j + 1]) < 2) and (abs(self.notes[j] - self.notes[j + 2]) < 2):
                result += 3


        """Check that notes in the allowed border"""
        for i in self.notes:
            if not self.in_range(i):
                result += 100

        return result

    def print_all_information(self):
        print("notes =", self.notes)
        print("velocity =", self.velocity)
        print("fitness =", self.fitness)
        print("local_max_notes =", self.local_max_notes)
        print("local_max_fitness =", self.local_max_fitness)
