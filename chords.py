import random
from constants import *


major = [0, 2, 4, 5, 7, 9, 11]
minor = [0, 2, 3, 5, 7, 8, 10]
accords_start = [0, 3, 4, 7, 10]


class Component:
    notes = None
    velocity = None
    fitness = None
    local_max_fitness = None
    local_max_notes = None
    note_pull = None
    tone = None

    def __init__(self, note_pull, tone):
        self.note_pull = note_pull
        self.tone = tone
        self.notes = [random.choice(note_pull) for i in range(48)]
        for i in range(0, 48, 3):
            self.notes[i], self.notes[i + 1], self.notes[i + 2] = sorted([self.notes[i],
                                                                          self.notes[i + 1],
                                                                          self.notes[i + 2]])
        self.local_max_notes = self.notes.copy()
        self.velocity = [0] * 48
        self.fitness = self.fitness_function()
        self.local_max_fitness = self.fitness

    def evolution(self, global_max):
        for i in range(48):
            self.velocity[i] = W * random.uniform(0, 1) * self.velocity[i] + \
                               C1 * random.uniform(0, 1) * (self.local_max_notes[i] - self.notes[i]) + \
                               C2 * random.uniform(0, 1) * (global_max[i] - self.notes[i])
            self.notes[i] = self.notes[i] + self.velocity[i]
        self.fitness = self.fitness_function()
        if self.local_max_fitness > self.fitness:
            self.local_max_fitness = self.fitness
            self.local_max_notes = self.notes

    def in_range(self, x):
        return (x > 48) and (x < 72)

    def closest_note(self, x):
        result = INF
        for note in self.note_pull:
            result = min(result, abs(note - x))
        return result

    def fitness_function(self):
        result = 0

        """Check that chords are correct"""
        for acc0, acc1, acc2 in zip(self.notes[0::3], self.notes[1::3], self.notes[2::3]):
            pre_result = INF
            for a_s in accords_start:
                    apre_result = min(pre_result, (abs(acc0 % 24 - self.tone[a_s]) +
                                                  abs(acc1 % 24 - self.tone[a_s + 2]) +
                                                  abs(acc2 % 24 - self.tone[a_s + 4])) * 3)
            result += pre_result

        """Check that last chord is tonic"""
        sorted_last = sorted([self.notes[45], self.notes[46], self.notes[47]])
        result += min(4 * (abs(sorted_last[0] % 24 - self.tone[0]) +
                                 abs(sorted_last[1] % 24 - self.tone[2]) +
                                 abs(sorted_last[2] % 24 - self.tone[4])),
                            4 * (abs(sorted_last[0] % 24 - self.tone[7]) +
                                 abs(sorted_last[1] % 24 - self.tone[9]) +
                                 abs(sorted_last[2] % 24 - self.tone[11])))

        """Check difference between adjacent chords"""
        for z in range(3, 48, 3):
            ch1 = self.notes[z - 2]
            ch2 = self.notes[z + 1]
            if abs(ch1 - ch2) > 12:
                result += 2 * (abs(ch1 - ch2) - 12)

        """Check that there are no 4 similar chords that following each other"""
        for z in range(0, 39, 3):
            if (abs(min(self.notes[z], self.notes[z + 1], self.notes[z + 2]) -
                           min(self.notes[z + 3], self.notes[z + 4], self.notes[z + 5])) < 2) and \
                    (abs(min(self.notes[z], self.notes[z + 1], self.notes[z + 2]) -
                            min(self.notes[z + 6], self.notes[z + 7], self.notes[z + 8])) < 2) and \
                    (abs(min(self.notes[z], self.notes[z + 1], self.notes[z + 2]) -
                             min(self.notes[z + 9], self.notes[z + 10], self.notes[z + 11])) < 2):
                result += 30

        """Check that notes in the allowed border"""
        for i in self.notes:
            if not self.in_range(i):
                result += 100

        return result

    def print_all_information(self):
        print("notes =", self.notes)
        print("velocity =", self.velocity)
        print("fitness =", self.fitness)
        print("local_max_notes =", self.local_max_notes)
        print("local_max_fitness =", self.local_max_fitness)
