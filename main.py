import PSO
import time
def str_to_int(x):
    try:
        return int(x)
    except:
        return -1

print("Choose tonality: 0..23.\n0 = C\n1 = Cm\n2 = Cis\n3 = Cism\n...")

q = str_to_int(input())
while not (q >= 0 and q <= 23):
    print("Incorrect input, try again.")
    q = str_to_int(input())
tonalit = q
print("Is music should be standart?\n0 = Yes\n1 = No")

q = input()
while not (q != 0 or q != 1):
    print("Incorrect input, try again.")
    q = str_to_int(input())
print(q)
improve_midi = q
t = time.time()
PSO.make_music(tonalit, improve_midi)
print(time.time() - t)